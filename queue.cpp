#include "queue.h"


Queue::Queue() {
    front = NULL;
    end = NULL;
}

Queue::~Queue() {
    delete front;
    delete end;
}

void Queue::push(int x1, int y1) {  //push node(x1,y1) into the queue

    Node *node1 = new Node();
    node1->x=x1;
    node1->y=y1;

    node1->next = NULL;    //con
    if(front == NULL){
        front = node1;
    }
    else{
        end->next = node1; // node dung sau node end hien tai
    }
                         // gia tri end moi cho nam sau cung
    end = node1;         //update new value for the end node
  //  std::cout<<"x_push o ham push:"<<node1->x<<std::endl;
   // std::cout<<"y_push o ham push:"<<node1->y<<std::endl;
}

Node Queue::pop() {      //pop node top out of the queue
    Node *node2 = new Node();
    Node value;
    if(front == NULL){
        std::cout<<"\n emtpty queue!!!\n";
    }
    else{
        node2 = front;          // lay ra thang top
        value.x=node2->x;
        value.y=node2->y;

        front = front->next;    //update value of new top
        delete node2;           // phai xoa node2
    }
   // std::cout<<"x_pop o ham pop:"<<value.x<<std::endl;
   // std::cout<<"y pop o ham pop:"<<value.y<<std::endl;

    return value;
}

bool Queue::isEmpty() {
    if(front==NULL) return true;
    return false;

}

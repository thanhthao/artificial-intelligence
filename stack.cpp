#include <iostream>
#include "search.h"
#include "stack.h"

Stack::Stack(){
    top = NULL;
}

Stack::~Stack(){
    delete top;
}



void Stack::push(int x1, int y1){ // push node(x1,y1) into stack

    Node1 *p;
    p = new Node1;
    p->x=x1;
    p->y=y1;

    p->next = NULL;
    if(top!=NULL){
        p->next = top;     // next la node ke phia duoi no, top o tren dau
    }
    top = p;               // update new value for the top
    // std::cout<<"x_push in push():"<<p->x<<std::endl;
    //std::cout<<"y_push in push():"<<p->y<<std::endl;

}

Node1 Stack::pop(){  // pop node top out of the stack

    Node1 *temp;
    Node1 value;

    if(top==NULL){
        std::cout<<"\n empty stack"<<std::endl;
    }else{
        temp = top;
        top = top->next;
        value.x=temp->x;
        //std::cout<<"x_pop in pop():"<<value.x<<std::endl;
        value.y=temp->y;
        // std::cout<<"y_pop in pop():"<<value.y<<std::endl;
        delete temp;
    }
    return value;
}

bool Stack::isEmpty(){
    if(top==NULL) return true;
    return false;

}

// da sua

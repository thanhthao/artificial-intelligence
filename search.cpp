#include "search.h"
#include "stack.h"
#include "queue.h"


Search::Search()
{
    m=0;
    n=0;

}

Search ::~Search()
{

}

// check x2,y2 co phai la con cua x1,y1 khong
// check that node(x2,y2) is a child of node(x1,y1) or not???
bool Search::isChildUp(int x1, int y1, int x2, int y2)
{
    if(x2 == x1-1 && y2 == y1)     return true;  //  up(-1,0)
    return false;


}

bool Search::isChildLeft(int x1, int y1, int x2, int y2)
{

    if(x2 == x1 && y2 == y1-1)  return true; // left (0,-1)
    return false;


}

bool Search::isChildDown(int x1, int y1, int x2, int y2)
{

    if(x2 == x1+1 && y2 == y1)  return true; // down(1,0)

    return false;


}

bool Search::isChildRight(int x1, int y1, int x2, int y2)
{

    if(x2 == x1 && y2 == y1+1) return true; // right(0,1)

    return false;

}

//search according to depth-first algorithm from node(x1,y1) to node(x2,y2)
void Search::depthFirst(int x1, int y1, int x2, int y2){
    Stack s;
    int visited[9][6];          // m dong , n cot

    for(int i = 1; i <= 9; i++)
        for(int j=1;j <= 6; j++)
            visited[i][j] = 0;
    s.push(x1,y1);
    if(x1 == x2 && y1 == y2 ) return;

    std::cout << "Depth-first algorithm starts from ("<<x1<<","<<y1<<")to("<<x2<<","<<y2<<"):"<< std::endl;

    while(!s.isEmpty()){

        Node1 temp;
        temp = s.pop();
        for(int i = 1; i <= 9; i++)
            for(int j=1;j <= 6; j++)
            {
                if(temp.x==i && temp.y==j)
                    visited[i][j] = 1;
            }

        // std::cout <<"x_pop o ham depth first:"<<temp.x<<std::endl;
        // std::cout <<"y pop o ham depth first:"<<temp.y<<std::endl;

        if( temp.x==x2 && temp.y==y2){

            std::cout<<"("<<temp.x<<","<<temp.y<<") ";   // to print the last one
            break;
        }

        std::cout<<"("<<temp.x<<","<<temp.y<<") "; // print each node in the path

        for (int i = 1; i <=9 ; i++)
            for(int j = 1; j <=6; j++)
                if (isChildRight(temp.x,temp.y,i,j)==true && visited[i][j]==0) {
                    s.push(i,j);
                }

        for (int i = 1; i <=9 ; i++)
            for(int j = 1; j <=6; j++)
                if (isChildDown(temp.x,temp.y,i,j)==true && visited[i][j]==0) {
                    s.push(i,j);
                }
        for (int i = 1; i <=9 ; i++)
            for(int j = 1; j <=6; j++)
                if (isChildLeft(temp.x,temp.y,i,j)==true && visited[i][j]==0) {
                    s.push(i,j);
                }
        for (int i = 1; i <=9 ; i++)
            for(int j = 1; j <=6; j++)
                if (isChildUp(temp.x,temp.y,i,j)==true && visited[i][j]==0) {
                    s.push(i,j);
                }
        // stack: last in ,first out
        // change the order to: right, down,left, up
        // for loop phai di nguoc  neu  ko la sai vi stack last in,first out
        // visited =1 khi no duoc pop ra,ko phai push dzo
    }

    std::cout<<std::endl;
}

//search according to breadth-first algorithm from node(x1,y1) to node(x2,y2)
void Search::breadthFirst(int x1,int y1, int x2,int y2)
{
    Queue q;
    int visited[9][6]; // m dong , n cot

    for(int i = 1; i <= 9; i++)
        for(int j=1;j <= 6; j++)
            visited[i][j] = 0;
    q.push(x1,y1);
    if(x1 == x2 && y1 == y2 ) return;

    std::cout << "Breadth-first algorithm starts from ("<<x1<<","<<y1<<")to("<<x2<<","<<y2<<"):"<< std::endl;

    while(!q.isEmpty()){

        Node temp;
        temp = q.pop();
        for(int i = 1; i <= 9; i++)
            for(int j=1;j <= 6; j++)
            {
                if(temp.x==i && temp.y==j)
                    visited[i][j] = 1;
            }

        // std::cout <<"x_pop o ham breadth first:"<<temp.x<<std::endl;
        // std::cout <<"y pop o ham breadth first:"<<temp.y<<std::endl;

        if( temp.x==x2 && temp.y==y2){

            std::cout<<"("<<temp.x<<","<<temp.y<<") ";   // to print the goal
            break;
        }

        std::cout<<"("<<temp.x<<","<<temp.y<<") "; // print the path

        for (int i = 1; i <9 ; i++)
            for(int j = 1; j <=6; j++)
                if (isChildUp(temp.x,temp.y,i,j)==true && visited[i][j]==0) {
                    q.push(i,j);
                    visited[i][j] = 1;
                }

        for (int i = 1; i <9 ; i++)
            for(int j = 1; j <=6; j++)
                if (isChildLeft(temp.x,temp.y,i,j)==true && visited[i][j]==0) {
                    q.push(i,j);
                    visited[i][j] = 1;
                }

        for (int i = 1; i <9 ; i++)
            for(int j = 1; j <=6; j++)
                if (isChildDown(temp.x,temp.y,i,j)==true && visited[i][j]==0) {
                    q.push(i,j);
                    visited[i][j] = 1;
                }

        for (int i = 1; i <9 ; i++)
            for(int j = 1; j <=6; j++)
                if (isChildRight(temp.x,temp.y,i,j)==true && visited[i][j]==0) {
                    q.push(i,j);
                    visited[i][j] = 1;
                }

        // queue: fist in, first out
        // order : up,left,down,right
        // visited =1 khi no duoc push dzo
    }

    std::cout<<std::endl;
}

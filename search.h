#ifndef SEARCH_H
#define SEARCH_H

class Search
{

private:
    int m, n;


public:
    Search();
    ~Search();
    bool isChildUp(int x1 , int y1, int x2, int y2);
    bool isChildLeft(int x1 , int y1, int x2, int y2);
    bool isChildDown(int x1 , int y1, int x2, int y2);
    bool isChildRight(int x1 , int y1, int x2, int y2);

    void depthFirst(int x1, int y1,int x2, int y2);
    void breadthFirst(int x1, int y1,int x2, int y2);


};
#endif // SEARCH_H

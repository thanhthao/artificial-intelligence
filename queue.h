#ifndef QUEUE_H
#define QUEUE_H

#include <iostream>


struct Node {
    int x;             // the y coordinate,  row
    int y;             // the x coordinate , column
    Node *next;
};

class Queue {

    private:
        Node *front;
        Node *end; // sau
    public:
        Queue();
        ~Queue();
        bool isEmpty();
        void push(int x1, int y1);
        Node pop();


};

#endif // QUEUE_H
